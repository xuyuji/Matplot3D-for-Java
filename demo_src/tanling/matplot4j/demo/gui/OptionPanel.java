package tanling.matplot4j.demo.gui;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import tanling.matplot_4j.comment.SysConstents;
import tanling.matplot_4j.d3d.base.element.DotStyle;
import tanling.matplot_4j.d3d.base.engine.Point3D;
import tanling.matplot_4j.d3d.facade.MatPlot3DMgr;
import tanling.matplot_4j.gui.LegendPanel;
import tanling.matplot_4j.style.ColorHelper;

public class OptionPanel extends JPanel implements Observer {

	 private  boolean isAntiAliasing = false;
	
	 private  boolean isMouseDraggable = true;
	
	// --------------------------------------

	private double seeta;

	private double beita;

	private double unitX;

	private double unitY;

	private double unitZ;

	private double scaleX;

	private double scaleY;

	private double scaleZ;

	private Point3D focusPoint = new Point3D(0, 0, 0);

	// 焦点透视
	private boolean isFocusPerspective = true;

	// --------------------------------
	// 行间距
	private int rGap = 5;

	private int rHeigth = 19;

	private int xBorder = 12;

	private int yBorder = 26;

	// 列宽
	private int[] vWidths = new int[] { 105, 173, 150 };

	// ------------------------------------
	// 抗锯齿
	private  JCheckBox checkAntiAliasing = new JCheckBox("AntiAliasing  抗锯齿");

	// 鼠标交互操作
	private  JCheckBox checkMouseDraggable = new JCheckBox("MouseDraggable  鼠标交互操作");

	private JCheckBox checkShowReferencePlanes = new JCheckBox("ShowReferencePlanes  显示参考平面");

	private JRadioButton raScatterPerspective = new JRadioButton("ScatterPerspective  散点透视");
	private JRadioButton raFocusPerspective = new JRadioButton("FocusPerspective  交点透视");

	private JTextField seetaTextField = new JTextField();

	private JTextField beitaTextField = new JTextField();

	// private JTextField unitXTextField=new JTextField();
	//
	// private JTextField unitYTextField=new JTextField();
	//
	// private JTextField unitZTextField=new JTextField();

	private JTextField scaleXTextField = new JTextField();

	private JTextField scaleYTextField = new JTextField();

	private JTextField scaleZTextField = new JTextField();

	// -------------------------------------------------

	private AbsContentPanel holder;

	public OptionPanel(AbsContentPanel holder) {

		this.holder = holder;

		this.setLayout(null);
		this.setBackground(Color.WHITE);
		this.setPreferredSize(new Dimension(300, 800));
		this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(ColorHelper.LIGHT_BLUE),
				"Observation parameters"));

		ButtonGroup bg = new ButtonGroup();
		bg.add(this.raFocusPerspective);
		bg.add(this.raScatterPerspective);
		raFocusPerspective.setSelected(true);

		this.add(checkAntiAliasing, 0, 0, 1, 2);
		this.add(checkMouseDraggable, 1, 0, 1, 2);
		checkMouseDraggable.setSelected(true);

		this.add(raFocusPerspective, 3, 0, 1, 2);
		this.add(raScatterPerspective, 4, 0, 1, 2);

		this.add(new JLabel("seeta"), 6, 0);
		this.add(seetaTextField, 6, 1);

		this.add(new JLabel("beita"), 7, 0);
		this.add(beitaTextField, 7, 1);

		this.add(new JLabel("scaleX"), 9, 0);
		this.add(scaleXTextField, 9, 1);

		this.add(new JLabel("scaleY"), 10, 0);
		this.add(scaleYTextField, 10, 1);

		this.add(new JLabel("scaleZ"), 11, 0);
		this.add(scaleZTextField, 11, 1);

		this.add(checkShowReferencePlanes, 13, 0, 1, 2);
		checkShowReferencePlanes.setSelected(true);

		// this.add(new JLabel("unitX"),13,0);
		// this.add(unitXTextField,13,1);
		//
		// this.add(new JLabel("unitY"),14,0);
		// this.add(unitYTextField,14,1);
		//
		// this.add(new JLabel("unitZ"),15,0);
		// this.add(unitZTextField,15,1);

		initListener();

	}

	private void initListener() {

		this.checkMouseDraggable.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				holder.getDemoFrame().getMainPanel().setGlobalMouseDraggable(checkMouseDraggable.isSelected());

			}
		});

		this.checkAntiAliasing.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				holder.getDemoFrame().getMainPanel().setGlobalAntiAliasing(checkAntiAliasing.isSelected());
				try {
					holder.mgr.updateView(2);
				} catch (InterruptedException e1) {
				}
			}
		});

		this.checkShowReferencePlanes.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				holder.mgr.setShowReferencePlanes(checkShowReferencePlanes.isSelected());
				try {
					holder.mgr.updateView(2);
				} catch (InterruptedException e1) {
				}
			}
		});

		raScatterPerspective.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				holder.mgr.setScatterPerspectiveType(true);

				try {
					holder.mgr.updateView(2);
				} catch (InterruptedException e1) {
				}
			}
		});

		raFocusPerspective.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				holder.mgr.setFocusPerspectiveType(true);

				try {
					holder.mgr.updateView(2);
				} catch (InterruptedException e1) {
				}
			}
		});

		seetaTextField.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					double d = Double.valueOf(seetaTextField.getText());
					holder.mgr.setSeeta(d);
					holder.mgr.updateView(2);
				} catch (Exception ex) {
				}
			}
		});

		beitaTextField.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					double d = Double.valueOf(beitaTextField.getText());
					holder.mgr.setBeita(d);
					holder.mgr.updateView(2);
				} catch (Exception ex) {
				}
			}
		});

		scaleXTextField.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					double d = Double.valueOf(scaleXTextField.getText());
					holder.mgr.setScaleX(d);
					holder.mgr.updateView(2);
				} catch (Exception ex) {
				}
			}
		});

		scaleYTextField.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					double d = Double.valueOf(scaleYTextField.getText());
					holder.mgr.setScaleY(d);
					holder.mgr.updateView(2);
				} catch (Exception ex) {
				}
			}
		});

		scaleZTextField.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					double d = Double.valueOf(scaleZTextField.getText());
					holder.mgr.setScaleZ(d);
					holder.mgr.updateView(2);
				} catch (Exception ex) {
				}
			}
		});

		// unitXTextField.addActionListener(new ActionListener() {
		//
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// try{
		// double d=Double.valueOf(unitXTextField.getText());
		// holder.mgr.setUnitX(d);
		// holder.mgr.updateView(2);
		// }catch(Exception ex){
		// }
		// }
		// });
		//
		// unitYTextField.addActionListener(new ActionListener() {
		//
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// try{
		// double d=Double.valueOf(unitYTextField.getText());
		// holder.mgr.setUnitY(d);
		// holder.mgr.updateView(2);
		// }catch(Exception ex){
		// }
		// }
		// });
		//
		// unitZTextField.addActionListener(new ActionListener() {
		//
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// try{
		// double d=Double.valueOf(unitZTextField.getText());
		// holder.mgr.setUnitZ(d);
		// holder.mgr.updateView(2);
		// }catch(Exception ex){
		// }
		// }
		// });
	}

	public void add(JComponent comp, int r, int c) {

		add(comp, r, c, 1, 1);
	}

	public void add(JComponent comp, int r, int c, int spanRow, int SpanColumn) {

		this.add(comp);
		comp.setFont(SysConstents.TEXT_FONT);
		comp.setBackground(Color.WHITE);
		comp.setBorder(BorderFactory.createEmptyBorder());

		int y = r * (rHeigth + rGap) + yBorder;

		int x = 0;

		if (c <= 0) {
			x = xBorder;
		} else {
			x = this.vWidths[c - 1] + xBorder;
		}

		int width = 0;

		for (int b = c, i = 0; i < SpanColumn; i++) {
			width += vWidths[i];
		}

		int heigth = this.rHeigth * spanRow + this.rGap * (spanRow - 1);

		comp.setBounds(x, y, width, heigth);
	}

	private void updateSelf() {
		this.beitaTextField.setText(String.format("%.2f", this.beita));
		this.seetaTextField.setText(String.format("%.2f", this.seeta));

		this.scaleXTextField.setText(String.format("%.2f", this.scaleX));
		this.scaleYTextField.setText(String.format("%.2f", this.scaleY));
		this.scaleZTextField.setText(String.format("%.2f", this.scaleZ));

		// this.unitXTextField.setText(String.format("%.2f",this.unitX));
		// this.unitYTextField.setText(String.format("%.2f",this.unitY));
		// this.unitZTextField.setText(String.format("%.2f",this.unitZ));
	}

	private void updateView(MatPlot3DMgr mgr) {

		this.beita = mgr.getBeita();
		this.seeta = mgr.getSeeta();

		this.scaleX = mgr.getScaleX();
		this.scaleY = mgr.getScaleY();
		this.scaleZ = mgr.getScaleZ();

		this.unitX = mgr.getUnitX();
		this.unitY = mgr.getUnitX();
		this.unitZ = mgr.getUnitX();

		updateSelf();

	}

	public void update(Observable o, Object arg) {
		MatPlot3DMgr mgr = (MatPlot3DMgr) o;
		updateView(mgr);
	}

	public  boolean isAntiAliasing() {
		return isAntiAliasing;
	}

	public  void setAntiAliasing(boolean isAntiAliasing) {
		this.isAntiAliasing = isAntiAliasing;
		this.checkAntiAliasing.setSelected(isAntiAliasing);
	}

	public  boolean isMouseDraggable() {
		return isMouseDraggable;
	}

	public  void setMouseDraggable(boolean isMouseDraggable) {
		this.isMouseDraggable = isMouseDraggable;
		this.checkMouseDraggable.setSelected(isMouseDraggable);
	}

	public static void main(String[] args) {
		JFrame jf = new JFrame("Matplot for JAVA");

		OptionPanel p = new OptionPanel(null);

		jf.setContentPane(p);

		jf.setDefaultCloseOperation(jf.EXIT_ON_CLOSE);
		jf.setSize(1000, 800);

		jf.setVisible(true);

	}
}
