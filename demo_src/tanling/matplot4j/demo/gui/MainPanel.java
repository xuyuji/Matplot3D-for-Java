package tanling.matplot4j.demo.gui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

public class MainPanel extends JPanel {
	//������ά����
	public static final int FUNCTION_FURFACE=1;
	
	public static final int BARS_PLANE=2;
	
	public static final int CURVES_PLANE=3;
	
	public static final int FUNCTION_POINTS=4;
	
	public static final int DOTS=5;
	
	//=======================================
	
	private DemoFrame demoFrame;
	
	private FunctionFurfaceContentPanel furfaceContentPanel;
	
	private FunctionPointsContentPanel functionPointsContentPanel;
	
	private BarsContentPanel barsContentPanel;
	
	private Curves2DContentPanel curves2DContentPanel;
	
	private DotsContentPanel dotsContentPanel;

	private AbsContentPanel currentShowPanel;
	
	private List<AbsContentPanel> contentPanels=new ArrayList<AbsContentPanel>();
	
	//=======================================
		
	private CardLayout card=new CardLayout();
	
	public MainPanel(DemoFrame demoFrame){
		this.demoFrame= demoFrame;
		
		this.setLayout(card);
		
		this.furfaceContentPanel=new FunctionFurfaceContentPanel(demoFrame);
		this.add(furfaceContentPanel,FUNCTION_FURFACE+"");
		contentPanels.add(furfaceContentPanel);
		
		this.functionPointsContentPanel=new FunctionPointsContentPanel(demoFrame);
		this.add( functionPointsContentPanel,FUNCTION_POINTS+"");
		contentPanels.add(functionPointsContentPanel);
		
		this.barsContentPanel=new BarsContentPanel(demoFrame);
		this.add( barsContentPanel,BARS_PLANE+"");
		contentPanels.add(barsContentPanel);
		
		this.curves2DContentPanel=new Curves2DContentPanel(demoFrame);
		this.add( curves2DContentPanel,CURVES_PLANE+"");
		contentPanels.add(curves2DContentPanel);
		
		this.dotsContentPanel=new DotsContentPanel(demoFrame);
		this.add( dotsContentPanel,DOTS+"");
		contentPanels.add(dotsContentPanel);

		
	}
	
	public void setType(int type){
		
		this.card.show(this, type+"");

	}

	public AbsContentPanel getCurrentShowPanel() {
		return currentShowPanel;
	}
	
	public void setGlobalAntiAliasing(boolean b){
		for(int i=0;i<this.contentPanels.size();i++){
			contentPanels.get(i).getOptionPanel().setAntiAliasing(b);
			contentPanels.get(i).getMatPlot3DMgr().setAntiAliasing(b);
		}
	}
	
	public void setGlobalMouseDraggable(boolean b){
		for(int i=0;i<this.contentPanels.size();i++){
			contentPanels.get(i).getOptionPanel().setMouseDraggable(b);
			contentPanels.get(i).getMatPlot3DMgr().setMouseDraggable(b);
		}
	}
}
