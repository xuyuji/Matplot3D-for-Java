package tanling.matplot4j.demo;

import java.util.Enumeration;

import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

import tanling.matplot_4j.comment.SysConstents;

public class UIUtil {
	public static void initGlobalFont(){
	    FontUIResource fontUIResource = new FontUIResource(SysConstents.DEFAULT_FONT);
	    for (Enumeration keys = UIManager.getDefaults().keys(); keys.hasMoreElements();) {
	        Object key = keys.nextElement();
	        Object value= UIManager.get(key);
	        if (value instanceof FontUIResource) {
	            UIManager.put(key, fontUIResource);
	        }  
	    }
	}
}
