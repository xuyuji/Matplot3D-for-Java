package tanling.matplot4j.demo.gui;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import tanling.matplot4j.demo.TextFileHelper;
import tanling.matplot_4j.d3d.base.engine.Point3D;
import tanling.matplot_4j.d3d.base.engine.Range;
import tanling.matplot_4j.d3d.facade.Function;
import tanling.matplot_4j.d3d.facade.MatPlot3DMgr;
import tanling.matplot_4j.gui.MpfjDrawLayer;

public class DotsContentPanel extends AbsContentPanel {

	public DotsContentPanel(DemoFrame frame) {
		super(frame, "散点空间分布");
	}

	@Override
	public void init() {
		try {
			mgr.setDataInputType(MatPlot3DMgr.DATA_TYPE_DOTS);

			BufferedReader br;

//			String path = this.getClass().getClassLoader().getResource("demo_resource").getPath();
//			System.out.println(path);

			br = new BufferedReader(new InputStreamReader(
					this.getClass().getClassLoader().getResourceAsStream("demo_resource/dots_data 2.txt")));

			String str = null;
			int num = -1;

			Map<Integer, List<Point3D>> map = new HashMap<Integer, List<Point3D>>();

			while (true) {
				str = br.readLine();

				if (str == null)
					break;

				char c = str.charAt(0);

				if (c == '=') {
					String s = str.substring(1);
					num = Integer.valueOf(s);

					map.put(num, new ArrayList<Point3D>());

				} else {
					String[] strs = str.split("\t");

					Point3D point = new Point3D(Double.valueOf(strs[0]), Double.valueOf(strs[1]),
							Double.valueOf(strs[2]));

					map.get(num).add(point);
				}
			}

			Iterator<Integer> it = map.keySet().iterator();

			while (it.hasNext()) {

				Integer integer = it.next();

				List<Point3D> li = map.get(integer);

				mgr.addData(integer.toString(), li);

			}

			mgr.setScaleX(1.2);
			mgr.setScaleY(1.2);
			mgr.setScaleZ(1.2);

			mgr.setSeeta(0.6);
			mgr.setBeita(1.0);

			mgr.setTitle("Demo : MNIST 数据集机器学习分类问题数据 3维 空间分布");

			// ----------------------------------------

			this.textArea.setText(TextFileHelper.getText("demo_src_txt/DotrsDemoSrc.txt"));

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

}
