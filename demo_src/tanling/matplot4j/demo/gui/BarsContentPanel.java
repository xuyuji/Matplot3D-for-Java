package tanling.matplot4j.demo.gui;

import java.awt.BorderLayout;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import tanling.matplot4j.demo.TextFileHelper;
import tanling.matplot_4j.d3d.base.engine.Range;
import tanling.matplot_4j.d3d.facade.Function;
import tanling.matplot_4j.d3d.facade.MatPlot3DMgr;
import tanling.matplot_4j.gui.MpfjDrawLayer;

public class BarsContentPanel extends AbsContentPanel {

	public BarsContentPanel(DemoFrame frame) {
		super(frame, "柱状图阵列");
	}

	@Override
	public void init() {
		mgr.setDataInputType(MatPlot3DMgr.DATA_TYPE_MATRIX);

		Random ra=new Random();
		double[][] ds1 = new double[][] { { 3, 4, 5, 6, 7 }, { 2, 3, 4, 9, 6 }, { 1, 2, 3, 8, 5 } };

		double[][] ds2 = new double[][] {
				{ 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(), 6 + ra.nextDouble(),
						7 + ra.nextDouble() },
				{ 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(),
						6 + ra.nextDouble() },
				{ 1 + ra.nextDouble(), 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(),
						5 + ra.nextDouble() } };

		double[][] ds3 = new double[][] {
				{ 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(), 6 + ra.nextDouble(),
						7 + ra.nextDouble() },
				{ 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(),
						6 + ra.nextDouble() },
				{ 1 + ra.nextDouble(), 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(),
						5 + ra.nextDouble() } };

		double[][] ds4 = new double[][] {
				{ 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(), 6 + ra.nextDouble(),
						7 + ra.nextDouble() },
				{ 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(),
						6 + ra.nextDouble() },
				{ 1 + ra.nextDouble(), 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(),
						5 + ra.nextDouble() } };

		double[][] ds5 = new double[][] {
				{ 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(), 6 + ra.nextDouble(),
						7 + ra.nextDouble() },
				{ 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(), 5 + ra.nextDouble(),
						6 + ra.nextDouble() },
				{ 1 + ra.nextDouble(), 2 + ra.nextDouble(), 3 + ra.nextDouble(), 4 + ra.nextDouble(),
						5 + ra.nextDouble() } };

		mgr.addData("项目1", ds1);
		mgr.addData("项目2", ds2);
		mgr.addData("项目3", ds3);
		mgr.addData("项目4", ds4);
		mgr.addData("项目5", ds5);
		
		mgr.setTitle("Demo : 多层柱状图");

		mgr.setScaleZ(1.2);
		mgr.setScaleX(1.2);
		mgr.setScaleY(2);
		
		mgr.setSeeta(0.3);
		mgr.setBeita(1.2);
	
		//----------------------------------------
		
		this.textArea.setText(TextFileHelper.getText("demo_src_txt/BarsDemoSrc.txt"));
	
	}

}
