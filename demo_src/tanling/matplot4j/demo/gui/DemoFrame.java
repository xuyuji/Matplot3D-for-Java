package tanling.matplot4j.demo.gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.HeadlessException;
import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

import tanling.matplot4j.demo.UIUtil;
import tanling.matplot_4j.comment.SysConstents;
/**
 * @preserve public
 */
public class DemoFrame extends JFrame {
	
	private MainToolBar toolBar=new MainToolBar(this);

	private MainPanel mainPanel;

	public DemoFrame(String title) {
		super(title);
		this.mainPanel=new MainPanel(this);
		
		JPanel contentPanel=new JPanel();
		contentPanel.setLayout(new BorderLayout());

		this.setContentPane(contentPanel);
		
		mainPanel.setType(MainPanel.FUNCTION_FURFACE);
		
		contentPanel.add(toolBar,BorderLayout.NORTH);
		contentPanel.add(mainPanel,BorderLayout.CENTER);
	}

	public MainToolBar getToolBar() {
		return toolBar;
	}

	public MainPanel getMainPanel() {
		return mainPanel;
	}
	
	public static void main(String[] args) {
		
		DemoFrame jf = new DemoFrame("Matplot3D for Java  [ Demo ]");
		
		UIUtil.initGlobalFont();
		
		try {
//			 String plaf =
//					 "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
//					 UIManager.setLookAndFeel(plaf);
		
//					 String plaf = "javax.swing.plaf.metal.MetalLookAndFeel";
//					 UIManager.setLookAndFeel(plaf);

			
			SwingUtilities.updateComponentTreeUI(jf);
		} catch (Exception e) {
			e.printStackTrace();
		}

		jf.setDefaultCloseOperation(jf.EXIT_ON_CLOSE);
		jf.setSize(1300, 820);
		jf.setVisible(true);
		
		
	}	
}
