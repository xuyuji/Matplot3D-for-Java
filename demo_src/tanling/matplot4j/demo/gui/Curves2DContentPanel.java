package tanling.matplot4j.demo.gui;

import java.awt.BorderLayout;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import tanling.matplot4j.demo.TextFileHelper;
import tanling.matplot_4j.d3d.base.engine.Range;
import tanling.matplot_4j.d3d.facade.Function;
import tanling.matplot_4j.d3d.facade.MatPlot3DMgr;
import tanling.matplot_4j.gui.MpfjDrawLayer;

public class Curves2DContentPanel extends AbsContentPanel {

	public Curves2DContentPanel(DemoFrame frame) {
		super(frame, "多层2维曲线图");
	}

	@Override
	public void init() {
		mgr.setDataInputType(MatPlot3DMgr.DATA_TYPE_CURVE2DS);
		Random ra=new Random();
		
		List<Point2D.Double> li1 = new ArrayList<Point2D.Double>();

		for (int i = 0; i < 100; i++) {
			li1.add(new Point2D.Double(i, i + ra.nextDouble() * 0.2 * 100));
		}

		List<Point2D.Double> li2 = new ArrayList<Point2D.Double>();

		for (int i = 0; i < 100; i++) {
			li2.add(new Point2D.Double(i, 0.85 * i + ra.nextDouble() * 0.2 * 100));
		}

		List<Point2D.Double> li3 = new ArrayList<Point2D.Double>();

		for (int i = 0; i < 100; i++) {
			li3.add(new Point2D.Double(i, 0.6 * i + ra.nextDouble() * 0.2 * 100));
		}

		List<Point2D.Double> li4 = new ArrayList<Point2D.Double>();

		for (int i = 0; i < 100; i++) {
			li4.add(new Point2D.Double(i, 0.45 * i + ra.nextDouble() * 0.2 * 100));
		}

		List<Point2D.Double> li5 = new ArrayList<Point2D.Double>();

		for (int i = 0; i < 100; i++) {
			li5.add(new Point2D.Double(i, 0.3 * i + ra.nextDouble() * 0.2 * 100));
		}

		mgr.addData2D("Item系列 1", null, li1);
		mgr.addData2D("Item系列 2", null, li2);
		mgr.addData2D("Item系列 3", null, li3);
		mgr.addData2D("Item系列 4", null, li4);
		mgr.addData2D("Item系列 5", null, li5);

		mgr.setScaleY(1.5);
		
		mgr.setSeeta(0.15);
		mgr.setBeita(1.1);
		
		mgr.setTitle("Demo : 多层2维趋势图表");
		
		//----------------------------------------
		
		this.textArea.setText(TextFileHelper.getText("demo_src_txt/Curver2dsDemoSrc.txt"));
			
	}

}
