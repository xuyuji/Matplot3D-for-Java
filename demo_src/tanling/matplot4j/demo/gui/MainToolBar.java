/**
 * 
 */
package tanling.matplot4j.demo.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.border.BevelBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.DefaultMutableTreeNode;

import tanling.matplot4j.demo.TextFileHelper;

/**
 * @author TanLing
 * 
 */
public class MainToolBar extends JToolBar {

	private DemoFrame frame;

	// -----------------------------------

	private JButton bDots;

	private JButton bPlans;

	private JButton bFunctionPoints;

	private JButton bSurface;

	private JButton bBars;

	// ////////////////////////////////////////////////////////
	// �Ի���
	// private ContrDialog addContrDialog;
	//
	// private EditContractDialog editContrDialog;
	//
	// private NewDialog newDialog;

	public MainToolBar(final DemoFrame mainFrame) {
		super();
		this.frame = mainFrame;

		this.setFloatable(true);
		this.setBorder(BorderFactory.createEmptyBorder());

		bDots = new JButton(readImage("demo_image/dots_b.png"));
		bDots.setMaximumSize(new Dimension(51, 51));

		bPlans = new JButton(readImage("demo_image/plans_b.png"));
		bPlans.setMaximumSize(new Dimension(51, 51));

		bFunctionPoints = new JButton(readImage("demo_image/function_dots_b.png"));
		bFunctionPoints.setMaximumSize(new Dimension(51, 51));

		bSurface = new JButton(readImage("demo_image/function_furface_b.png"));
		bSurface.setMaximumSize(new Dimension(51, 51));

		bBars = new JButton(readImage("demo_image/bars_b.png"));
		bBars.setMaximumSize(new Dimension(51, 51));

		bDots.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getMainPanel().setType(MainPanel.DOTS);
			}
		});

		bSurface.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getMainPanel().setType(MainPanel.FUNCTION_FURFACE);
			}
		});

		bBars.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getMainPanel().setType(MainPanel.BARS_PLANE);
			}
		});

		bPlans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getMainPanel().setType(MainPanel.CURVES_PLANE);
			}
		});

		bFunctionPoints.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getMainPanel().setType(MainPanel.FUNCTION_POINTS);
			}
		});

		// this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.add(creatSeparatorPanel());
		
		this.add(this.bSurface);
		this.add(this.bDots);
		this.add(this.bPlans);
		this.add(this.bFunctionPoints);
		this.add(this.bBars);

		this.add(creatSeparatorPanel());

		this.setPreferredSize(new Dimension(1280, 52));

		// ��ʼ��״̬
		initToolBar();
	}

	private ImageIcon readImage(String dir) {
		try {
			InputStream is = TextFileHelper.class.getClassLoader().getResourceAsStream(dir);
			byte[] buffer = convertToByteArray(is);

			is.close();
			return new ImageIcon(buffer);
		} catch (IOException e) {
			return null;
		}

	}

	private byte[] convertToByteArray(final InputStream is) {
		int read = 0;
		int totalRead = 0;

		int bufferCount = 1024;
		List<Byte> bList = new ArrayList<Byte>();

		byte[] byteArray = new byte[bufferCount];
		try {
			while ((read = is.read(byteArray)) >= 0) {
				for (int i = 0; i < read; i++) {
					bList.add(byteArray[i]);
				}
			}

		} catch (Exception e) {
			return null;
		}

		byte[] rt = new byte[bList.size()];
		for (int i = 0; i < rt.length; i++) {
			rt[i] = bList.get(i);
		}
		return rt;
	}

	private JPanel creatSeparatorPanel() {
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(0, 3, 0, 3),
				BorderFactory.createLineBorder(Color.LIGHT_GRAY)));
		p.setMaximumSize(new Dimension(9, 45));
		p.setOpaque(false);
		return p;
	}

	public void initToolBar() {
		// setContractEditEnable(false);
		// setFileExsisedState(false);
	}

}
