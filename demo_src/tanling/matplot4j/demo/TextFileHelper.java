package tanling.matplot4j.demo;

import java.io.BufferedReader;

import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;


public class TextFileHelper {
	public static String getText(String filePath){
		
		try {
			
//			String path = this.getClass().getClassLoader().getResource("demo_resource").getPath();
//			System.out.println(path);
			
			InputStream ips=TextFileHelper.class.getClassLoader().getResourceAsStream(filePath);
			
			BufferedReader reader=new BufferedReader(new InputStreamReader(ips));
			
			String buffer;
			
			StringBuffer sb=new StringBuffer();
			
			int i=0;
			
			while(true){
				
				buffer=reader.readLine();
				
				if(buffer==null)
					break;
			
				sb.append(buffer);
				sb.append("\n");
			}

			return sb.toString();
			
		} catch (Exception e) {
			return null;
		}
		
	}
}
