package tanling.matplot4j.demo.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import tanling.matplot4j.demo.TextFileHelper;
import tanling.matplot_4j.d3d.base.engine.Range;
import tanling.matplot_4j.d3d.facade.Function;
import tanling.matplot_4j.d3d.facade.MatPlot3DMgr;
import tanling.matplot_4j.gui.MpfjDrawLayer;

public class FunctionFurfaceContentPanel extends AbsContentPanel{
	
	public FunctionFurfaceContentPanel(DemoFrame frame){
		super(frame, "函数曲面");
	}

	@Override
	public void init() {
		mgr.setDataInputType(MatPlot3DMgr.DATA_TYPE_FUNCTION3D);

		Function f = new Function() {

			public double f(double x, double y) {
				 return Math.sin(y*x/2.2)*0.8;

//				return Math.sin(y) * Math.cos(x) * 0.8;
			}

		};

		mgr.addData(f, new Range(-6, 6), new Range(-6, 6), 50, 50);

		mgr.setScaleZ(0.5);
		mgr.setScaleX(1.3);
		mgr.setScaleY(1.3);
		
		mgr.setSeeta(1.3);
		mgr.setBeita(1.1);
		
		mgr.setTitle("Demo : 函数曲面绘制   [ z =  0.8 * sin(y*x/2.2) ]");
		
		//----------------------------------------
		
		this.textArea.setText(TextFileHelper.getText("demo_src_txt/SurfaceDemoSrc.txt"));
	}

}
