package tanling.matplot4j.demo.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.TextArea;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import tanling.matplot_4j.comment.SysConstents;
import tanling.matplot_4j.d3d.base.engine.Range;
import tanling.matplot_4j.d3d.facade.Function;
import tanling.matplot_4j.d3d.facade.MatPlot3DMgr;
import tanling.matplot_4j.gui.MpfjDrawLayer;
import tanling.matplot_4j.style.ColorHelper;

public abstract class AbsContentPanel extends JPanel {
	protected DemoFrame demoFrame;
	
	protected JTabbedPane tab = new JTabbedPane();

	protected OptionPanel optionPanel ;

	protected JPanel showPanel;

	protected JPanel srcPanel = new JPanel();

	protected TextArea textArea = new TextArea();
	// --------------------------------------------

	protected MatPlot3DMgr mgr = new MatPlot3DMgr();

	public AbsContentPanel(DemoFrame frame, String tabName) {

		this.demoFrame=frame;
		
		this.setLayout(new BorderLayout());
		this.add(tab, BorderLayout.CENTER);
		
		this.optionPanel = new OptionPanel(this);
		//********************************
		//加入观察者
		mgr.addObserver(optionPanel);

		tab.setFont(SysConstents.DEFAULT_FONT);

		init();

		this.showPanel = mgr.getPanel();
				
		 showPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(2,
		 2, 2, 1), BorderFactory.createLineBorder(ColorHelper.LIGHT_BLUE)));

		JPanel buffer = new JPanel(new BorderLayout());
		buffer.add(showPanel, BorderLayout.CENTER);
		buffer.add(optionPanel, BorderLayout.EAST);
		// ----------------------------------------------------

		textArea.setFont(SysConstents.DEFAULT_FONT);
		textArea.setEditable(false);
		textArea.setBackground(Color.WHITE);

		srcPanel.setLayout(new BorderLayout());
		srcPanel.add(textArea, BorderLayout.CENTER);

		tab.setTabLayoutPolicy(JTabbedPane.WRAP_TAB_LAYOUT);
		tab.addTab(tabName, buffer);
		tab.addTab("源代码   (sorce code)", srcPanel);

	}

	public abstract void init();

	public MatPlot3DMgr getMatPlot3DMgr() {
		return mgr;
	}

	public OptionPanel getOptionPanel() {
		return optionPanel;
	}

	public DemoFrame getDemoFrame() {
		return demoFrame;
	}

}
